/**
 * 
 */
package com.social.controller;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.mail.javamail.JavaMailSender;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import com.social.entity.*;

import javax.mail.MessagingException;

/**
 * @author HS
 *
 */
@RestController
public class EmailController {
	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

	@RequestMapping(value = "/sendemail", method = RequestMethod.GET)
	@ResponseBody
	public String sendEmail() throws MessagingException, IOException {
		
		
		Map<String, Object> mailmap = new HashMap<String, Object>();
		
		mailmap.put("mailType", "REGN"); // whether new user regn or activation etc. 
		mailmap.put("from", "ravi@api360.world");
		mailmap.put("mailTo", "ravi.dhyani8881@gmail.com");
		mailmap.put("subject", "API-360 newsletter subscription");
		//mailmap.put("spring.mail.port", "465");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("name", "Friend");
		model.put("location", "India");
		model.put("sign", "HS");
		mailmap.put("props", model);

		MailParams mailparams = new MailParams();
		mailparams.setMailType((String) (mailmap.get("mailType")));
		mailparams.setFrom((String) (mailmap.get("from")));
		mailparams.setMailTo((String) (mailmap.get("mailTo")));
		mailparams.setSubject((String) (mailmap.get("subject")));
		
		
		mailparams.setProps((Map<String, Object>) (mailmap.get("props")));

		
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());
		helper.addAttachment("template-cover.png", new ClassPathResource("educational-resource.jpg"));
		Context context = new Context();
		context.setVariables(mailparams.getProps());
		
		String html = "";
		//---- This check will be modified depending upon type of email, whether registration or activation etc.
		if(mailparams.getMailType().equals("REGN")) {
			 html = templateEngine.process("testmail", context);
		} else {
			 html = templateEngine.process("testmail-one", context);
		}
		
		helper.setTo(mailparams.getMailTo());
		helper.setText(html, true);
		helper.setSubject(mailparams.getSubject());
		helper.setFrom(mailparams.getFrom());
		try {
			javaMailSender.send(message);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return "mail Sent successfully";
	}

	public JavaMailSender getJavaMailSender() {
		return javaMailSender;
	}

	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
}

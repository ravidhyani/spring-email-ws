package com.social.entity;

public class ClientData {

	private Integer id;
	private String msg;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ClientData(Integer id, String msg) {
		super();
		this.id = id;
		this.msg = msg;
	}
	
	
}

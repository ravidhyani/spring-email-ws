package com.social;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmailWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmailWsApplication.class, args);
	}
}
